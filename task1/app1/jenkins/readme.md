# Run latest jenkins with this below command to get docker command support inside the Jenkins container 


docker run -u 0 --privileged --name jenkins -it -d -p 8080:8080 -p 5000:5000 \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker):/usr/bin/docker \
-v /home/jenkins_home:/var/jenkins_home \
jenkins/jenkins:lts

run -u 0 --privileged 
it means we will run docker with privileged user(root). So that we can run docker command inside Jenkins container.


-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker):/usr/bin/docker \
	As we not install docker in Jenkins image, to build docker image, we will use container host machine’s docker. For this reason, we pointed container host machine’s docker to the Jenkins container. 

-v /home/jenkins_home:/var/jenkins_home \
	We will host Jenkins home directory in our container host machine so that , we don’t lose any data if er restart the container. 

**** Docker Compose is testing for this command at the moment ****

